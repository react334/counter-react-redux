import { configureStore } from '@reduxjs/toolkit';
import counterReducer from './reducers/counterReducer';
import { state } from './state';

const store = configureStore({
  reducer: counterReducer,
  devTools: process.env.NODE_ENV !== 'production',
  preloadedState: state,
})

export default store;
