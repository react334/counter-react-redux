import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { incrementAction, decrementAction } from '../store/actions/index';

import Button from '../Button';
import './Counter.css';

const Counter = () => {
  const count = useSelector(state => state.count)
  const dispatch = useDispatch();

  return (
    <div className="counter">
      <span>
        Counter:
      </span>
      <Button value="+" onClick={() => dispatch(incrementAction())} />
        {count}
      <Button value="-" onClick={() => dispatch(decrementAction())} />
    </div>
  );
};

export default Counter;
